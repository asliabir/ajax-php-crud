// loading data to index page
$(document).ready(function () {
    loadData();
});
// loading data function
function loadData() {
    $.ajax({
        url: 'bend/read.php',
        method: 'GET',
        success: function (responce) {
            responce = $.parseJSON(responce);
            if (responce.status == 'success') {
                $('#dataTable').html(responce.table);
            }
        }
    });
}

// getting data from input form of create data
$(document).ready(function () {
    // checking all input and disable the submit button untill all form filluped
    var setup = true;
    $('#form1').on('change', function (e) {
        e.preventDefault();
        var form = $("#form1");
        var checkbox = $("input[name='gender']").is(":checked");
        var agr = form.find('#agr');
        agr = agr.prop("checked");
        if (validate(form) && agr && checkbox) {
            document.getElementById("createData").disabled = false;
        } else {
            document.getElementById("createData").disabled = true;
        }
    });
    // form validation
    $('#form1').on('change', function (e) {
        e.preventDefault();
        var form = $("#form1");
        // first name checking
        $("input[name='fname']").keyup(function () {
            // first name and values
            var fname = form.find('#fname');
            var lname = form.find('#lname');
            var fnameVal = form.find('#fname').val().length;
            if (fnameVal <= 30) {
                $('#form1').find('#wfname').html('');
                fname.removeClass('is-invalid');
                fname.addClass('is-valid');
                var setup = true;
            } else {
                $('#form1').find('#wfname').html('Name only allow less then 30 char!');
                fname.addClass('is-invalid');
                fname.removeClass('is-valid');
                lname.addClass('is-invalid');
                lname.removeClass('is-valid');
                var setup = false;
            }
        });
        // last name checking
        $("input[name='lname']").change(function () {
            // first name and values
            var fname = form.find('#fname');
            var lname = form.find('#lname');
            var lnameVal = form.find('#lname').val().length;
            if (lnameVal <= 30) {
                $('#form1').find('#wfname').html('');
                lname.removeClass('is-invalid');
                lname.addClass('is-valid');
                var setup = true;
            } else {
                $('#form1').find('#wfname').html('Name only allow less then 30 char!');
                fname.addClass('is-invalid');
                fname.removeClass('is-valid');
                lname.addClass('is-invalid');
                lname.removeClass('is-valid');
                var setup = false;
            }
        });
        // email checking
        $("input[name='email']").change(function () {
            // first name and values
            var email = form.find('#email');
            var emailVal = form.find('#email').val();
            var atposition = emailVal.indexOf("@");
            var dotposition = emailVal.lastIndexOf(".");
            if (atposition < 1 || dotposition < atposition + 2 || dotposition + 2 >= emailVal.length) {
                $('#form1').find('#wemail').html('Please enter a valid email!');
                email.addClass('is-invalid');
                email.removeClass('is-valid');
                var setup = false;
            } else {
                $('#form1').find('#wemail').html('');
                email.removeClass('is-invalid');
                email.addClass('is-valid');
                var setup = true;

            }
        });
        // password checking
        $("#password1, #password2").keyup(function () {
            // first name and values
            var password1 = form.find("#password1");
            var password2 = form.find("#password2");
            var pass1Val = password1.val();
            var pass2Val = password2.val();
            
            // var passVal = password.val();
            var match = false;
            // password matching check
            if (pass1Val != pass2Val){
                $('#form1').find('#wpass').html('password not matched');
                password1.removeClass('is-valid');
                password1.addClass('is-invalid');
                password2.removeClass('is-valid');
                password2.addClass('is-invalid');
                match = false;
                var setup = false;
            } else {
                $('#form1').find('#wpass').html('');
                password1.removeClass('is-invalid');
                password1.addClass('is-valid');
                password2.removeClass('is-invalid');
                password2.addClass('is-valid');
                match = true;
                var setup = true;
            }
            // password lenght checking
            if(match && (pass1Val.length > 30 || pass1Val.length < 6)){
                $('#form1').find('#wpass').html('Password can\'t be more then character 30 or less then 6 charecter! ');
                password1.removeClass('is-valid');
                password1.addClass('is-invalid');
                password2.removeClass('is-valid');
                password2.addClass('is-invalid');
                var setup = false;
            }
        });
    });
    // sending data to database
    $('#form1').on('submit', function (e) {
        e.preventDefault();
        var form = $("#form1");
        var data = $("#form1").serialize();
        var checkbox = $("input[name='gender']").is(":checked");
        if (validate(form) && checkbox && setup) {
            createData(data);
            $('#form1')[0].reset();
        } else {
            var alert = $('#alert2');
            alert.addClass('alert');
            alert.addClass('alert-warning');
            alert.html('Please fill all form!');
        }
    });
});

// function to create data and save to database and send response
function createData(data) {
    $.ajax({
        url: 'bend/add.php',
        type: 'POST',
        data: data,
        success: function (responce) {
            responce = $.parseJSON(responce);
            // condition for alert and load data
            if (responce.status == 'success') {
                var alert = $('#alert');
                alert.addClass('alert');
                alert.addClass('alert-success');
                alert.html(responce.message);
                $('.modal').each(function () {
                    $(this).modal('hide');
                });
                $("#alert").fadeTo(2000, 500).slideUp(500, function () {
                    $("#alert").slideUp(500);
                    $('#fname').removeClass('is-valid');
                    $('#lname').removeClass('is-valid');
                    $('#password1').removeClass('is-valid');
                    $('#password2').removeClass('is-valid');
                    $('#email').removeClass('is-valid');
                });
            } else {
                var alert = $('#alert');
                alert.addClass('alert');
                alert.addClass('alert-danger');
                alert.html(responce.message);
            }
            // loading data
            loadData();
        }
    });
}

// checking table click data to purform delete or edit or vide action
$(function () {
    $('table').on('click', function (e) {
        e.preventDefault();
        var tag = $(e.target).parent('#update');
        var id = tag.attr('data-id');
        if (tag.hasClass('edit')) {
            getData(id);
        }
        var dTag = $(e.target).parent('#delete');
        var dId = dTag.attr('data-id');
        if (dTag.hasClass('delete')) {
            deleteData(dId);
        }

        var vTag = $(e.target).parent('#view');
        var vId = vTag.attr('data-id');
        if (vTag.hasClass('view')) {
            viewData(vId);
        }
    });
});
// getting data to show
function getData(id) {
    $.ajax({
        url: 'bend/getData.php',
        method: 'post',
        data: {
            id: id
        },
        success: function (responce) {
            responce = JSON.parse(responce);
            if (responce.status == 'success') {
                var modal = $('#editModal');
                var form = modal.find('#form2');
                form.find('#id').val(responce[0].id);
                form.find('#fname').val(responce[0].fname);
                form.find('#lname').val(responce[0].lname);
                form.find('#email').val(responce[0].email);
                form.find('#password1').val(responce[0].password);
                form.find('#password2').val(responce[0].password);
                // checking gender to make chacked
                var g = form.find('#gender');
                if (responce[0].gender == 'male') {
                    g.filter("[value='male']").attr('checked', true);
                } else if (responce[0].gender == 'female') {
                    g.filter("[value='female']").attr('checked', true);
                } else {
                    g.filter("[value='other']").attr('checked', true);
                }
                form.find('#dob').val(responce[0].dob);
                form.find('#education').val(responce[0].education);
                form.find('#address').val(responce[0].address);
                form.find('#bio').val(responce[0].bio);
            }

        }
    });
}

//updating data
function updateData(data) {
    $.ajax({
        url: 'bend/edit.php',
        type: 'POST',
        data: data,
        success: function (responce) {
            responce = $.parseJSON(responce);
            // condition for alert and load data
            if (responce.status == 'success') {
                var alert = $('#alert');
                alert.addClass('alert');
                alert.addClass('alert-success');
                alert.html(responce.message);
                $('.modal').each(function () {
                    $(this).modal('hide');
                });
                $("#alert").fadeTo(2000, 500).slideUp(500, function () {
                    $("#alert").slideUp(500);
                });
            } else {
                var alert = $('#alert');
                alert.addClass('alert');
                alert.addClass('alert-danger');
                alert.html(responce.message);
            }
            // loading data
            loadData();
        }
    });
}
// sending data if submit button clicked in form2 or data editing
$(document).ready(function () {
    var setup = true;
    $('#form2').keydown(function (e) {
        // button enable
        document.getElementById("editData").disabled = false;
    });
    $("input[name='gender']").change(function (e) {
        // button enable
        document.getElementById("editData").disabled = false;
    });
    // first name checking
    $("input[name='fname']").change(function () {
        var form = $('#form2');
        // first name and values
        var fname = form.find('#fname');
        var lname = form.find('#lname');
        var fnameVal = form.find('#fname').val().length;
        if (fnameVal <= 30) {
            $('#form2').find('#wfname').html('');
            fname.removeClass('is-invalid');
            fname.addClass('is-valid');
            setup = true;
        } else {
            $('#form2').find('#wfname').html('Name only allow less then 30 char!');
            fname.addClass('is-invalid');
            fname.removeClass('is-valid');
            lname.addClass('is-invalid');
            lname.removeClass('is-valid');
            setup = false;
        }
    });

    // last name checking
    $("input[name='lname']").change(function () {
        var form = $('#form2');
        // first name and values
        var fname = form.find('#fname');
        var lname = form.find('#lname');
        var lnameVal = form.find('#lname').val().length;
        if (lnameVal <= 30) {
            $('#form2').find('#wfname').html('');
            lname.removeClass('is-invalid');
            lname.addClass('is-valid');
            setup = true;
        } else {
            $('#form2').find('#wfname').html('Name only allow less then 30 char!');
            fname.addClass('is-invalid');
            fname.removeClass('is-valid');
            lname.addClass('is-invalid');
            lname.removeClass('is-valid');
            setup = false;
        }
    });
    // email checking
    $("input[name='email']").change(function () {
        var form = $('#form2');
        // first name and values
        var email = form.find('#email');
        var emailVal = form.find('#email').val();
        var atposition = emailVal.indexOf("@");
        var dotposition = emailVal.lastIndexOf(".");
        if (atposition < 1 || dotposition < atposition + 2 || dotposition + 2 >= emailVal.length) {
            $('#form2').find('#wemail').html('Please enter a valid email!');
            email.addClass('is-invalid');
            email.removeClass('is-valid');
            setup = false;
            $('#editData').prop('disabled', true);
        } else {
            $('#form2').find('#wemail').html('');
            email.removeClass('is-invalid');
            email.addClass('is-valid');
            setup = true;

        }
    });

    // password checking
    $("#password1, #password2").keyup(function () {
        var form = $('#form2');
        // first name and values
        var password1 = form.find("#password1");
        var password2 = form.find("#password2");
        var pass1Val = password1.val();
        var pass2Val = password2.val();
        var match = false;
        // password matching check
        if (pass1Val != pass2Val) {
            $('#editData').prop('disabled', true);
            $('#form2').find('#wpass').html('password not matched');
            password1.removeClass('is-valid');
            password1.addClass('is-invalid');
            password2.removeClass('is-valid');
            password2.addClass('is-invalid');
            match = false;
            setup = false;
        } else {
            $('#editData').prop('disabled', false);
            $('#form2').find('#wpass').html('');
            password1.removeClass('is-invalid');
            password1.addClass('is-valid');
            password2.removeClass('is-invalid');
            password2.addClass('is-valid');
            match = true;
            setup = true;
        }
        // password lenght checking
        if (match && (pass1Val.length > 30 || pass1Val.length < 6)) {
            $('#editData').prop('disabled', true);
            $('#form2').find('#wpass').html('Password can\'t be more then character 30 or less then 6 charecter! ');
            password1.removeClass('is-valid');
            password1.addClass('is-invalid');
            password2.removeClass('is-valid');
            password2.addClass('is-invalid');
            setup = false;
        }
    });

// submiting data
    $('#form2').on('submit', function (e) {
        e.preventDefault();
        var form = $("#form2");
        var data = $("#form2").serialize();
        if (validate(form) && setup) {
            updateData(data);
        } else {
            var alert = $('#alert3');
            alert.addClass('alert');
            alert.addClass('alert-warning');
            alert.html('Please fill all form!');
        }
    });
});

// delete function load
function deleteData(id) {
    $('#form3').on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            url: 'bend/delete.php',
            method: 'post',
            data: {
                id: id
            },
            success: function (responce) {
                responce = $.parseJSON(responce);
                // condition for alert and load data
                if (responce.status == 'success') {
                    var alert = $('#alert');
                    alert.addClass('alert');
                    alert.addClass('alert-danger');
                    alert.html(responce.message);
                    $('.modal').each(function () {
                        $(this).modal('hide');
                    });
                    $("#alert").fadeTo(2000, 500).slideUp(500, function () {
                        $("#alert").slideUp(500);
                    });
                } else {
                    var alert = $('#alert');
                    alert.addClass('alert');
                    alert.addClass('alert-warning');
                    alert.html(responce.message);
                }
                // loading data
                loadData();
            }
        });
    });
}
// hide modal

function modalHide() {
    $('.modal').each(function () {
        e.preventDefault();
        $(this).modal('hide');
    });
}

// view data 

// getting data to show
function viewData(id) {
    $.ajax({
        url: 'bend/getData.php',
        method: 'post',
        data: {
            id: id
        },
        success: function (responce) {
            responce = $.parseJSON(responce);
            if (responce.status == 'success') {
                var modal = $('#viewModal');

                modal.find('#fname').html(responce[0].fname);
                modal.find('#lname').html(responce[0].lname);
                modal.find('#email').html(responce[0].email);
                modal.find('#gender').html(responce[0].gender);
                modal.find('#dob').html(responce[0].dob);
                modal.find('#education').html(responce[0].education);
                modal.find('#address').html(responce[0].address);
                modal.find('#bio').html(responce[0].bio);
            }

        }
    });
}
// form validation check
function validate(form) {
    var inputTag = form.find('input');
    for (var i = 0; i < inputTag.length; i++) {
        if (inputTag[i].value == '' || inputTag[i].value == null) {
            return false;
        }
    }
    return true;
}